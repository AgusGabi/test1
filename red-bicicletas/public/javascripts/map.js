var map = L.map("main_map").setView([-33.691288, -59.650456], 13);

L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution:
    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
}).addTo(map);

L.marker([-33.691288, -59.650456]).addTo(map).openPopup();

$.ajax({
  dataType: "json",
  url: "api/bicicletas",
  success: function (result) {
    console.log(result);
    result.bicicleta.forEach((bici) => {
      L.marker(bici.ubicacion, { title: bici.id }).addTo(map);
    });
  },
});
